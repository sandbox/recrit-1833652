<?php

/**
 * @file
 * A specific handler for Military addresses
 */

$plugin = array(
  'title' => t('Military'),
  'format callback' => 'addressfield_format_address_military_generate',
  'type' => 'address',
  'weight' => -80,
);

/**
 * Format callback
 */
function addressfield_format_address_military_generate(&$format, $address, $context = array()) {
  if ($context['mode'] != 'form' || empty($address['country'])) {
    return;
  }

  switch ($address['country']) {
    case 'US':
      if (isset($format['locality_block']['administrative_area'])) {
        $state_options = array(
          'AE' => t('Armed Forces: Europe, Middle East, Africa, and Canada'),
          'AP' => t('Armed Forces: Pacific'),
          'AA' => t('Armed Forces: Americas excluding Canada'),
        );

        if (!empty($format['locality_block']['administrative_area']['#options'])) {
          $state_options += $format['locality_block']['administrative_area']['#options'];
        }

        $format['locality_block']['administrative_area']['#options'] = $state_options;
      }
      break;
  }
}
